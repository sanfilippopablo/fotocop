package integrationtests;
import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import common.exceptions.NotFoundException;
//import testutils.IntegrationBase;
import users.data.UsersService;
import users.entities.User;
import jobs.data.FilesService;
import jobs.data.JobsService;
import jobs.entities.File;
import jobs.entities.Job;
import testutils.IntegrationBase;

public class FilesServiceIT extends IntegrationBase {
	FilesService filesService;
	
	@Before
	public void setUp() {
		this.filesService = new FilesService();
	}
	
	@Test
	public void testCreateFileFromInputStreamAndGetInputStreamForFile() throws SQLException, IOException {
		String testString = "El alto TP";
		String name = "alto_tp.txt";
		InputStream stream = new ByteArrayInputStream(testString.getBytes(StandardCharsets.UTF_8));

		File file = filesService.createFileFromInputStream(stream, name);
		
		assertNotNull("Debería haber un file", file);
		assertNotNull("Debería tener un id", file.getId());
		
		String theString;
		try (InputStream is = filesService.getInputStreamForFile(file)) {
			theString = IOUtils.toString(is, StandardCharsets.UTF_8);
		}
		
		assertEquals("El contendio debe haberse guardado y volver correctamente", testString, theString);
		
	}
	
	@Test
	public void testGetFileByIdWithPublicFile() throws NotFoundException, SQLException {
		File file = null;
		file = filesService.getFileById(4); // El 4 es el único publico
		assertNotNull("It should have got a file", file);
		assertEquals("It should be the right one", 4, file.getId());
		assertEquals("It should have an uploader", 1, file.getUploader().getId());
		assertNotNull("It should have a title", file.getTitle());
		assertNotNull("It should have tags", file.getTags());
		assertEquals("It should have 3 tags", 3, file.getTags().size());
		assertTrue("It should be public", file.isPublic());
	}
	
	@Test
	public void testGetFileByIdWithPrivateFile() throws NotFoundException, SQLException {
		File file = null;
		file = filesService.getFileById(1);
		assertNotNull("It should have got a file", file);
		assertFalse("It should be private", file.isPublic());
		assertNull("It shouldn't have title", file.getTitle());
		assertNull("It shouldn't have tags", file.getTags());
		
	}
	
	@Test(expected=NotFoundException.class)
	public void testGetFileByIdWithIncorrectId() throws NotFoundException, SQLException {
		File file = filesService.getFileById(89);
	}

}

package integrationtests;

import testutils.IntegrationBase;
import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
//import testutils.IntegrationBase;
import users.data.UsersService;
import users.entities.User;
import users.exceptions.AuthException;
import jobs.entities.File;
import library.data.LibraryService;

public class LibraryServiceIT extends IntegrationBase {

	LibraryService libraryService;

	@Before
	public void setUp() {
		this.libraryService = new LibraryService();
	}
	@Test
	public void testUploadFile() throws SQLException, IOException, AuthException {
		UsersService us = new UsersService();
		String testString = "El alto TP";
		String name = "alto_tp.txt";
		InputStream stream = new ByteArrayInputStream(testString.getBytes(StandardCharsets.UTF_8));
		String title = "Es un alto TP que hice con los pibes";
		String tags = "alto tp, utn, frro";
		User uploader = us.getUserById(1);
		//The moment of the truth
		File file = libraryService.uploadFile(stream, name, title, tags, uploader);
		
		assertNotNull("Debería haber un file", file);
		assertNotNull("Debería tener un id", file.getId());

	}
	
	@Test
	public void testIdentifyTagsFromString(){
		String test_string = "testing  identification  of tags";
		ArrayList<String> tags = libraryService.identifyTagsFromString(test_string);
		
		assertEquals("Debería haber 4 tags", 4, tags.size()	);
	}
	
	@Test
	public void testFindFile () throws SQLException {
		ArrayList<File> files = libraryService.findFiles("fotocop");
		assertEquals("Debería haber sólo uno", 1, files.size());
		assertEquals("Debería ser el 4", 4, files.get(0).getId());
		files = libraryService.findFiles("hola chau newton");
		assertEquals("Debería encontrar dos", 2, files.size());
		assertEquals("Debería estar primero el apunte 4", 4, files.get(0).getId());
		assertEquals("El segundo debería ser el 5", 5, files.get(1).getId());
	}

}

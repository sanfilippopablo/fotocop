<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
	<head>
  		<c:import url="/user/partials/head.html" charEncoding="UTF-8"/>
  		<link rel="stylesheet" href="/user/styles/jobs.css" />
  	</head>

  	<body>

    	<c:import url="/user/partials/header.jsp" charEncoding="UTF-8"/>

  		<main class="grey lighten-3">
  			<div class="container">
  				<hgroup>
  					<div class="row">
  						<div class="title">
  							<div class="item item-primary">
  								<h4>Trabajos</h4>
  							</div>
  							<a href="/createJob" class="waves-effect waves-light red btn-large add"><i class="material-icons left">add</i> Agregar</a>
  						</div>
  					</div>
  				</hgroup>
  				<hr />
  				<br />
  				<div>
  					<c:choose>
		 				<c:when test="${fn:length(jobs) gt 0}">
							<c:forEach items="${jobs}" var="job">
		
				        		<c:if test='${job.getStatus() == "Abierto"}'>
				              		<div class="col s12 m6 l4">
				      					<div class="card" style="border-left: 5px solid blue;">
				      						<div class="card-content flex">
				      							<div class="card-principal-contenido">
				      								<div class="flex">
				      									<i class="material-icons circle blue icon-card">schedule</i>
				      									<h4 class="card-title center black-text text-darken-2"><c:out value='${job.getId()}' /></h4>
				      								</div>
				      								<p class="center flow-text blue-text block">Todavía no enviado.</p>
				      							</div>
				      							<a href="/job?id=<c:out value='${job.getId()}' />" class="waves-effect waves-light btn">Detalles</a>
				      						</div>
				      					</div>
				      				</div>
				            	</c:if>
				
				            	<c:if test='${job.getStatus() == "Enviado"}'>
				              		<div class="col s12 m6 l4">
				      					<div class="card" style="border-left: 5px solid green">
				      						<div class="card-content flex">
				      							<div class="card-principal-contenido">
				      								<div class="flex">
				      									<i class="material-icons circle green icon-card">done</i>
				      									<h4 class="card-title center black-text text-darken-2"><c:out value='${job.getId()}' /></h4>
				      								</div>
				      								<p class="center flow-text green-text block"> Pasar a retirar por fotocopiadora el <c:out value='${job.getEta()}' /></p>
				      							</div>
				      							<a href="/job?id=<c:out value='${job.getId()}' />" class="waves-effect waves-light btn">Detalles</a>
				      						</div>
				      					</div>
				      				</div>
				            	</c:if>
				
				            	<c:if test='${job.getStatus() == "Listo"}'>
				              		<div class="col s12 m6 l4">
				      					<div class="card" style="border-left: 5px solid red;">
				      						<div class="card-content flex">
				      							<div class="card-principal-contenido">
				      								<div class="flex">
				      									<i class="material-icons circle red icon-card">done_all</i>
				      									<h4 class="card-title center black-text text-darken-2"><c:out value='${job.getId()}' /></h4>
				      								</div>
				      								<p class="center flow-text red-text block">Listo para retirar por fotocopiadora.</p>
				      							</div>
				      							<a href="/job?id=<c:out value='${job.getId()}' />" class="waves-effect waves-light btn">Detalles</a>
				      						</div>
				      					</div>
				      				</div>
				            	</c:if>
			
			          		</c:forEach>

	  					</c:when>
	  					<c:otherwise>
		  					<div class="row">
		  						<div class="card-panel col s12 m6 offset-m3 blue darken-4">
		  							<h4 class="flow-text center-align white-text">No hay trabajos pendientes.</h4>
		  							<div class="center">
		  								<a href="/createJob" class="waves-effect waves-light red btn add">
		  									<i class="material-icons left">add</i> Crear nuevo trabajo
		  								</a>
		  							</div>
		  						</div>
		  					</div>
	          			</c:otherwise>
					</c:choose>
	          			
        		</div>
      		</div>
  		</main>

  		<!-- Scripts -->
  		<c:import url="/user/partials/scripts.html" charEncoding="UTF-8"/>
  		<script src="/user/scripts/jobs.js" type="text/javascript"></script>

	</body>

</html>
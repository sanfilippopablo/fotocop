<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<header>
  <nav class="teal">
    <div class="container">
      <div class="nav-wrapper">
        <a href="/" class="brand-logo">Fotocop</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <c:if test="${sessionScope.isLogged}">
            <li>
              <form class="row" action="/files/search" method="get">
                <input class="col s8" id="search" name="q" type="text" placeholder="Buscar apunte..." required />
                <label for="search" class="col s1"><i class="material-icons">search</i></label>
              </form>
            </li>
            <li class="active"><a href="#">${user.getUsername()}</a></li>
            <li><a href="/">Trabajos</a></li>
            <li><a href="/files/upload">Subir apunte</a></li>
            <li><a href="/logout">Salir</a></li>
          </c:if>
        </ul>
        <ul class="side-nav" id="mobile-demo">
          <c:if test="${sessionScope.isLogged}">
            <li>
              <form action="/files/search" method="get">
                <div class="row">
                  <input class="col s9 teal-text" id="search" name="q" type="text" placeholder="Buscar apunte..." required />
                  <label for="search" class="col s1"><i class="material-icons">search</i></label>
                </div>
              </form>
            </li>
            <li class="active"><a href="#">${user.getUsername()}</a></li>
            <li><a href="/">Trabajos</a></li>
            <li><a href="/files/upload">Subir apunte</a></li>
            <li><a href="/logout">Salir</a></li>
          </c:if>  
        </ul>
      </div>
    </div>
  </nav>
</header>
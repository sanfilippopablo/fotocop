<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<c:import url="/user/partials/head.html" charEncoding="UTF-8"/>
	<link rel="stylesheet" href="/user/styles/upload-file.css" />
</head>

<body>

	<c:import url="/user/partials/header.jsp" charEncoding="UTF-8"/>

	<main class="grey lighten-3">
		<div class="container">
			<hgroup>
				<div class="row">
					<div class="title">
						<div class="item item-primary">
							<h4>Guardar Apunte</h4>
						</div>
					</div>
				</div>
			</hgroup>
			<hr />
			<br />
			<div class="row">
					<form class="col s12 m6 l6 offset-m3 offset-l3 white" id="upload-file" method="POST" action="/files/upload" enctype="multipart/form-data">

						<div class="file-field input-field">
							<div class="btn">
								<span>Apunte</span>
								<input type="file" class="validate" name="newFile" id="newFile" required />
							</div>
							<div class="file-path-wrapper">
								<input class="file-path validate" type="text" name="filePath" id="filePath" placeholder="Pdf, txt, jpg." />
							</div>
							<div class="error error-newFile">
								<p class="red-text"><c:out value="${validationManager.errorForField('newFile')}"/></p>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<input id="file_title" name="file_title" type="text" class="validate" required />
								<label for="file_title">Título</label>
								<div class="error error-file_title">
									<c:out value="${validationManager.errorForField('file_title')}"/>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<input id="file_tags" name="file_tags" type="text" class="validate" required />
								<label for="file_tags">Tags</label>
								<div class="error error-file_tags">
									<p class="red-text"><c:out value="${validationManager.errorForField('file_tags')}"/></p>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col s12 center-align">
								<p class="red-text"><c:out value="${validationManager.errorForField('misc')}"/></p>
							</div>
						</div>

						<div class="row">
							<button class="waves-effect waves-teal btn btn-large col s12" type="submit" name="action">
								Guardar
							</button>
						</div>
					</form>
				</div>	
		</div>
	</main>

	<!-- Scripts -->
	<c:import url="/user/partials/scripts.html" charEncoding="UTF-8"/>
	<script type="text/javascript" src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="/user/scripts/upload-file.js" type="text/javascript"></script>

</body>

</html>
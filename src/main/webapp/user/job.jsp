<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
	<head>
		<c:import url="/user/partials/head.html" charEncoding="UTF-8"/>
		<link rel="stylesheet" href="/user/styles/job.css" />
	</head>
	<body>

    <c:import url="/user/partials/header.jsp" charEncoding="UTF-8"/>

		<main class="grey lighten-3">

	    	<div class="container">
	        	<hgroup>
					<div class="flex flex-text-buttom">
	              		<h4 class="title-work">Trabajo <strong><c:out value='${job.getId()}'/></strong></h4>
	              		<p class="state-work">Estado <strong><c:out value='${job.getStatus()}' /></strong></p>
	
						<c:if test='${job.getStatus() == "Abierto"}'>
	              			<a class="waves-effect waves-light modal-trigger red btn-large add" href="#newjobline"><i class="material-icons left">add</i> Agregar</a>
						</c:if>
	
					</div>
				</hgroup>
	          	<hr />
				<c:choose>
	 				<c:when test="${not empty job.getJobLines()}">
			          	<c:forEach items="${job.getJobLines()}" var="jobLine">
			  				<div class="col s12 m6 l4">
			  					<div class="card row">
					                	<div class="col s12 m12 l9">
					                		<h5 class="name-file"><c:out value='${jobLine.getFile().getName()}'/></h5>
				                		</div>
					                	<div class="col s12 m12 l3 center">
					                    	<p>Cantidad: <strong><c:out value='${jobLine.getQuantity()}'/></strong></p>
					                    	<p>Doble Faz: <strong><c:out value='${jobLine.isDobleFaz()}'/></strong></p>
					                    	<p>Abrochado: <strong><c:out value='${jobLine.isAbrochado()}'/></strong></p>
					                    	<p>Anillado: <strong><c:out value='${jobLine.isAnillado()}'/></strong></p>
					                	</div>
									<c:if test='${job.getStatus() == "Abierto"}'>
										<div class="col s12 m12 l12 center">
				  			        		<a class="waves-effect waves-light btn modal-trigger button-file" href="#modal<c:out value='${jobLine.getId()}'/>"><i class="material-icons">edit</i></a>
				  			        	</div>
									</c:if>
			
			              		</div>
			    			</div>
			    			
			    			<!-- Modal Structure -->
				          	<div id="modal<c:out value='${jobLine.getId()}'/>" class="modal">
				            	<div class="modal-content">
				                	<h4>Añadir archivo</h4>
				                  	<br />
				                  	<br />
				                  	<form action="/files/edit" id="editJobLine" method="post" enctype="multipart/form-data">
				                  		<input type="hidden" name="id" value="${job.getId()}" />
				                    	<div class="file-field input-field">
				                        	<div class="btn">
				                            	<span>Archivo</span>
				                              	<input type="file" name="">
				                          	</div>
				                          	<div class="file-path-wrapper">
				                            	<input class="file-path validate" type="text">
				                          	</div>
				                      	</div>
				                      	<div class="flex">
				                        	<div class="input-field">
				                            	<p>
				                                	<input type="checkbox" id="doble-faz" <c:if test="${jobLine.isDobleFaz()}">checked</c:if> />
				                                	<label for="doble-faz">Doble faz</label>
				                              	</p>
				                              	<p>
				                                	<input type="checkbox" id="anillado" <c:if test="${jobLine.isAnillado()}">checked</c:if> />
				                                	<label for="anillado">Anillado</label>
				                              	</p>
				                              	<p>
				                                	<input type="checkbox" id="abrochado" <c:if test="${jobLine.isAbrochado()}">checked</c:if> />
				                                	<label for="abrochado">Abrochado</label>
				                              	</p>
				                          	</div>
				                          	<br />
				                          	<div class="input-field">
				                            	<input id="copias" type="number" class="validate" value="<c:out value='${jobLine.getQuantity()}'/>" />
				                              	<label for="copias">Cantidad de copias</label>
				                          	</div>
				                      	</div>
				                  	</form>
				              	</div>
				              	<div class="modal-footer">
				              		<button type="submit" class="modal-action modal-close waves-effect waves-green btn-flat">Editar</button>
				                  	<button class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</button>
				              	</div>
				          	</div>
			          	</c:forEach>
		          	</c:when>
  					<c:otherwise>
  						<div class="row">
	  						<div class="card-panel col s12 m6 offset-m3 blue darken-4">
	  							<h4 class="flow-text center-align white-text">No hay archivo para imprimir.</h4>
	  							<div class="center">
	  								<a class="waves-effect waves-light modal-trigger red btn-large add" href="#newjobline">
	  									<i class="material-icons left">add</i> Añadir archivo
	  								</a>
	  							</div>
	  						</div>
		  				</div> 						
  					</c:otherwise>
				</c:choose>
	
				<c:if test='${job.getStatus() == "Abierto"}'>
			        <form class="flex flex-button" action="/job/send" method="post">
			        	<input type="hidden" name="idJob" value="${job.getId()}" />
			        	<button type="submit" class="waves-effect waves-light blue btn-large <c:if test="${empty job.getJobLines()}">disabled</c:if> send">Enviar</button>
			            <button type="button" class="waves-effect waves-light red btn-large cancelar">Cancelar</button>
			        </form>
				</c:if>
				
				<!-- Modal Structure -->
	          	<div id="newjobline" class="modal">
	            	<div class="modal-content">
	                	<h4>Añadir archivo</h4>
	                  	<br />
	                  	<br />
	                  	<form action="/files/add" method="post" id="newJobLine" enctype="multipart/form-data">
					        <input type="hidden" name="newId" value="${job.getId()}" />
	                    	<div class="file-field input-field">
	                        	<div class="btn">
	                            	<span>Archivo</span>
	                              	<input type="file" name="newFile">
	                          	</div>
	                          	<div class="file-path-wrapper">
	                            	<input class="file-path validate" name="filePath" id="filePath" type="text">
	                          	</div>
	                          	<div class="error error-newFile">
									<p class="red-text"><c:out value="${validationManager.errorForField('newFile')}"/></p>
								</div>
	                      	</div>
	                      	<div class="flex">
	                        	<div class="input-field">
	                            	<p>
	                                	<input type="checkbox" name="newDobleFaz" id="newDobleFaz" />
	                                	<label for="newDobleFaz">Doble faz</label>
	                              	</p>
	                              	<p>
	                                	<input type="checkbox" name="newAnillado" id="newAnillado" />
	                                	<label for="newAnillado">Anillado</label>
	                              	</p>
	                              	<p>
	                                	<input type="checkbox" name="newAbrochado" id="newAbrochado" />
	                                	<label for="newAbrochado">Abrochado</label>
	                              	</p>
	                          	</div>
	                          	<div class="row">
									<div class="col s12 center-align">
										<p class="red-text"><c:out value="${validationManager.errorForField('misc')}"/></p>
									</div>
								</div>
	                          	<br />
	                          	<div class="input-field">
	                            	<input id="newQuantity" type="number" name="newQuantity" class="validate" value="1" />
	                              	<label for="newQuantity">Cantidad de copias</label>
	                              	<div class="error error-newFile">
										<p class="red-text"><c:out value="${validationManager.errorForField('newQuantity')}"/></p>
									</div>
	                          	</div>
	                      	</div>
	                      	<br />
	                      	<br />
	                      	<div class="flex">
	                      		<button type="submit" class="modal-action waves-effect waves-green btn-flat">Agregar</button>
		                      	<button type="button" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</button>
	                		</div>
	                  	</form>
	              	</div>
	          	</div>
	      	</div>
		</main>

		<!-- Scripts -->
    	<c:import url="/user/partials/scripts.html" charEncoding="UTF-8"/>
    	<script type="text/javascript" src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
    	<script src="/user/scripts/job.js" type="text/javascript"></script>

	</body>
</html>
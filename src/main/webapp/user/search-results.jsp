<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
	<head>
  		<c:import url="/user/partials/head.html" charEncoding="UTF-8"/>
  		<link rel="stylesheet" href="/user/styles/search-results.css" />
  	</head>

  	<body>

    	<c:import url="/user/partials/header.jsp" charEncoding="UTF-8"/>

  		<main class="grey lighten-3">
  			<div class="container">
  				<hgroup>
  					<div class="row">
  						<div class="title">
  							<div class="item item-primary">
  								<h4>Resultados de búsqueda</h4>
  							</div>
  						</div>
  					</div>
  				</hgroup>
  				<hr />
  				<br />
  				<div>
  					<c:choose>
		 				<c:when test="${fn:length(files) gt 0}">
							<c:forEach items="${files}" var="file">
			              		<div class="col s12 m6 l4">
			      					<div class="card" style="border-left: 5px solid blue;">
			      						<div class="card-content flex">
			      							<div class="card-principal-contenido">
			      								<div class="flex">
			      									<h4 class="card-title center black-text text-darken-2"><c:out value='${file.getTitle()}' /></h4>
			      								</div>
			      								<div class="center">
				      								<c:forEach items="${file.getTags()}" var="tag">
				      									<div class="chip">
															<a href="/files/search?q=<c:out value='${tag}' />" class="center flow-text blue-text block"><c:out value='${tag}' /></a>
															<i class="material-icons">close</i>
														</div>  
				      								</c:forEach>
			      								</div>
			      								<br />
			      								<div class="center">
				      								<a class="waves-effect waves-light btn modal-trigger" href="#modal<c:out value='${file.getId()}'/>">Imprimir</a>
				      								<a href="/files/download?id=<c:out value='${file.getId()}' />" class="wares-effect wares-light btn">Descargar</a>
			      								</div>
			      							</div>	
			      						</div>
			      					</div>
			      				</div>

			      				<!-- Modal Structure -->
					          	<div id="modal<c:out value='${file.getId()}'/>" class="modal">
					            	<div class="modal-content">
					                	<h5>Enviar a imprimir - <c:out value='${file.getTitle()}'/></h5>
					                  	<hr />
										<form action="/files/addfromlibrary" method="post" class="newJobLine">
									        <input type="hidden" name="fileID" value="${file.getId()}" />
					                      	<div class="flex-form">
					                        	<div class="input-field">
					                            	<p>
					                                	<input type="checkbox" id="newDobleFaz<c:out value='${file.getId()}'/>" name="newDobleFaz" />
					                                	<label for="newDobleFaz<c:out value='${file.getId()}'/>">Doble faz</label>
					                              	</p>
					                              	<p>
					                                	<input type="checkbox" id="newAnillado<c:out value='${file.getId()}'/>" name="newAnillado" />
					                                	<label for="newAnillado<c:out value='${file.getId()}'/>">Anillado</label>
					                              	</p>
					                              	<p>
					                                	<input type="checkbox" id="newAbrochado<c:out value='${file.getId()}'/>" name="newAbrochado" />
					                                	<label for="newAbrochado<c:out value='${file.getId()}'/>">Abrochado</label>
					                              	</p>
					                          	</div>
					                          	<div class="row">
													<div class="col s12 center-align">
														<p class="red-text"><c:out value="${validationManager.errorForField('misc')}"/></p>
													</div>
												</div>
					                          	<br />
					                          	<div class="input-field">
					                            	<input type="number" name="newQuantity" class="validate" value="1" />
					                              	<label for="newQuantity">Cantidad de copias</label>
					                              	<div class="error error-newFile">
														<p class="red-text"><c:out value="${validationManager.errorForField('newQuantity')}"/></p>
													</div>
					                          	</div>
					                      	</div>
					                      	<br />
					                      	<br />
					                      	<div class="flex-form">
					                      		<button type="submit" class="modal-action waves-effect waves-green btn-flat">Enviar</button>
						                      	<button type="button" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</button>
					                		</div>
					                  	</form>
					              	</div>
					          	</div>
			          		</c:forEach>

	  					</c:when>
	  					<c:otherwise>
		  					<div class="row">
		  						<div class="card-panel col s12 m6 offset-m3 blue darken-4">
		  							<h4 class="flow-text center-align white-text">No se encontraron archivos.</h4>
		  						</div>
		  					</div>
	          			</c:otherwise>
					</c:choose>
	          			
        		</div>
      		</div>
  		</main>

  		<!-- Scripts -->
  		<c:import url="/user/partials/scripts.html" charEncoding="UTF-8"/>
  		<script type="text/javascript" src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
  		<script src="/user/scripts/search-results.js" type="text/javascript"></script>

	</body>

</html>
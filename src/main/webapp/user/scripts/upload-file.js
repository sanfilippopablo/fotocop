$(document).ready(function(){

	$('#filePath').change(function () {
		var text = $( '#filePath' ).val().replace(/\.[^/.]+$/, "");
		$('#file_title').val( text ).focus();
	});

	$("#upload-file").validate({

		errorClass: 'invalid',
		validClass: "valid",

		errorPlacement: function (error, element) {
			element.next("label").attr("data-error", error.contents().text());
		},

		rules:
		{
			filePath:
			{
				required: true
			},
			file_title:
			{
				required: true,
				minlength: 3,
				maxlength: 20
			},
			file_tags:
			{
				required: true,
				minlength: 2,
				maxlength: 100
			},
		},
		messages:
		{
			filePath:
			{
				required: "Por favor, seleccione apunte."
			},
			file_title:
			{
				required: "Por favor, ingrese título.",
				minlength: "Mínimo {0} caracteres.",
				maxlength: "Máximo {0} caracteres."
			},
			file_tags:
			{
				required: "Por favor, ingrese los tags separados por espacio.",
				minlength: "Mínimo {0} caracteres.",
				maxlength: "Máximo {0} caracteres."
			},
		}
	});
});
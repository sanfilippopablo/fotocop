$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal-trigger').leanModal();

    $(".newJobLine").validate({

		errorClass: 'invalid',
		validClass: "valid",

		errorPlacement: function (error, element) {
			element.next("label").attr("data-error", error.contents().text());
		},

		rules:
		{
			newQuantity:
			{
				required: true,
				number: true
			}
		},
		messages:
		{
			newQuantity:
			{
				required: "Por favor, ingrese cantidad de copias.",
				number: "Solo número."
			}
		}
	});
});
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
	<c:import url="/user/partials/head.html" charEncoding="UTF-8"/>
	<link rel="stylesheet" href="/user/styles/upload-success.css" />
</head>

<body>

	<c:import url="/user/partials/header.jsp" charEncoding="UTF-8"/>

	<main class="grey lighten-3">
		<div class="container">
			<hgroup>
				<div class="row">
					<div class="title">
						<div class="item item-primary">
							<h4>Guardado con éxito el apunte</h4>
						</div>
					</div>
				</div>
			</hgroup>
			<hr />
			<br />
			<div class="row">
				<div class="center-align">
					<a href="/files/upload" class="waves-effect waves-light btn-large"><i class="large material-icons left">note_add</i> Guardar otro apunte</a>
					<a href="/" class="waves-effect waves-light btn-large"><i class="large material-icons left">replay	</i> Volver al inicio</a>
				</div>
			</div>
		</div>
	</main>

	<!-- Scripts -->
	<c:import url="/user/partials/scripts.html" charEncoding="UTF-8"/>
	<script src="/user/scripts/upload-success.js" type="text/javascript"></script>

</body>

</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" isErrorPage="true"%>
<!DOCTYPE html>
<html>
	<head>
  		<c:import url="/errors/partials/head.html" charEncoding="UTF-8"/>
  		<link rel="stylesheet" href="/errors/styles/errors.css" />
  	</head>

  	<body>

    	<c:import url="/errors/partials/header.jsp" charEncoding="UTF-8"/>

  		<main class="grey lighten-3">
  			<div class="container">
				<div class="row">
					<h4 class="center-align">Error 500</h4>
				</div>
				<div class="row">
					<h1 class="center-align">¯\_(ツ)_/¯</h1>
					<p class="center-align">Lo siento, error en el servidor.</p>
				</div>
        	</div>
  		</main>

  		<!-- Scripts -->
  		<c:import url="/errors/partials/scripts.html" charEncoding="UTF-8"/>
  		<script src="/errors/scripts/errors.js" type="text/javascript"></script>

	</body>

</html>
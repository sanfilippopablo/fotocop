<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
	<head>
  		<c:import url="/admin/partials/head.html" charEncoding="UTF-8"/>
  		<link rel="stylesheet" href="/admin/styles/jobs.css" />
  	</head>

  	<body>

    	<c:import url="/admin/partials/header.jsp" charEncoding="UTF-8"/>

  		<main class="grey lighten-3">
  			<div class="container">
  				<hgroup>
  					<div class="row">
  						<div class="title">
  							<div class="item item-primary">
  								<h4>Trabajos pendientes de imprimir o retirar</h4>
  							</div>
  						</div>
  					</div>
  				</hgroup>
  				<hr />
  				<br />
				<c:choose>
	 				<c:when test="${not empty jobs}">
			          	<c:forEach items="${jobs}" var="job">
			          		<c:if test='${job.getStatus() == "Enviado"}'>
				  				<div class="col s12">
				  					<div class="card">
				  						<div class="container">
						                	<div class="section">
						                		<div class="row valign-wrapper">
							                		<div class="col s12 m6 l6 valign">
							                			<h5>Trabajo <c:out value='${job.getId()}'/></h5>
							                		</div>
							                		<div class="col s12 m6 l6 valign right-align">
							                			<p><b>De:</b> <c:out value='${job.getUser().getUsername()}'/></p>
							                			<p><b>Entrega estimada:</b> <c:out value='${job.getEta()}'/></p>
							                		</div>
							                	</div>
						                	</div>
						                	<div class="divider"></div>
						                	<div class="section"> 
												<table class="striped">
													<thead>
														<tr>
															<th data-field="fileName">Archivo</th>
															<th data-field="quantity">Cantidad</th>
															<th data-field="dobleFaz">Doble Faz</th>
															<th data-field="abrochado">Abrochado</th>
															<th data-field="anillado">Anillado</th>
														</tr>
													</thead>

													<tbody>
														<c:forEach items="${job.getJobLines()}" var="jobLine">
															<tr>
																<td><c:out value='${jobLine.getFile().getName()}'/></td>
																<td><c:out value='${jobLine.getQuantity()}'/></td>
																<td><c:out value='${jobLine.isDobleFaz()}'/></td>
																<td><c:out value='${jobLine.isAbrochado()}'/></td>
																<td><c:out value='${jobLine.isAnillado()}'/></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
											<div class="divider"></div>
											<div class="section center-align">
						  			        	<form action="/files/print" method="post">
						  			        		<input type="hidden" name="id" value="${job.getId()}" />
													<button class="waves-effect waves-light btn" type="submit" name="filesPrint">Imprimir</button>
												</form>
											</div>
										</div>					
				              		</div>
				    			</div>
			    			</c:if>
			    			<c:if test='${job.getStatus() == "Listo"}'>
				  				<div class="col s12">
				  					<div class="card">
				  						<div class="container">
						                	<div class="section">
						                		<div class="row valign-wrapper">
							                		<div class="col s12 m6 l6 valign">
							                			<h5>Trabajo <c:out value='${job.getId()}'/></h5>
							                		</div>
							                		<div class="col s12 m6 l6 valign right-align">
							                			<p><b>De:</b> <c:out value='${job.getUser().getUsername()}'/></p>
							                		</div>
							                	</div>
						                	</div>
						                	<div class="divider"></div>
						                	<div class="section"> 
												<table class="striped">
													<thead>
														<tr>
															<th data-field="fileName">Archivo</th>
															<th data-field="quantity">Cantidad</th>
															<th data-field="dobleFaz">Doble Faz</th>
															<th data-field="abrochado">Abrochado</th>
															<th data-field="anillado">Anillado</th>
														</tr>
													</thead>

													<tbody>
														<c:forEach items="${job.getJobLines()}" var="jobLine">
															<tr>
																<td><c:out value='${jobLine.getFile().getName()}'/></td>
																<td><c:out value='${jobLine.getQuantity()}'/></td>
																<td><c:out value='${jobLine.isDobleFaz()}'/></td>
																<td><c:out value='${jobLine.isAbrochado()}'/></td>
																<td><c:out value='${jobLine.isAnillado()}'/></td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
											</div>
											<div class="divider"></div>
											<div class="section center-align">
						  			        	<form action="/job/retirado" method="post">
						  			        		<input type="hidden" name="id" value="${job.getId()}" />
													<button class="waves-effect waves-light  blue darken-3 btn" type="submit" name="filesPrint">Marcar como entregado</button>
												</form>
											</div>
										</div>					
				              		</div>
				    			</div>
			    			</c:if>
			    			<!-- Modal Structure -->
			          	</c:forEach>
		          	</c:when>
					<c:otherwise>
						<div class="row">
	  						<div class="card-panel col s12 m6 offset-m3 blue darken-4">
	  							<h4 class="flow-text center-align white-text">No hay trabajos.</h4>
	  						</div>
	  					</div> 						
					</c:otherwise>
				</c:choose>
	          			
        	</div>
  		</main>

  		<!-- Scripts -->
  		<c:import url="/admin/partials/scripts.html" charEncoding="UTF-8"/>
  		<script src="/admin/scripts/jobs.js" type="text/javascript"></script>

	</body>

</html>
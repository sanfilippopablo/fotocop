<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<header>
  <nav class="teal">
    <div class="container">
      <div class="nav-wrapper">
        <a href="/admin" class="brand-logo">Fotocop</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
		 	<c:if test="${sessionScope.isLogged}">
		    	<li class="active"><a href="#">${user.getUsername()}</a></li>
		    	<li><a href="/admin">Trabajos</a></li>
		    	<li><a href="/logout">Salir</a></li>
	 		</c:if>
        </ul>
        <ul class="side-nav" id="mobile-demo">
        	<c:if test="${sessionScope.isLogged}">
	    		<li class="active"><a href="#">${user.getUsername()}</a></li>
	    		<li><a href="/admin">Trabajos</a></li>
	    		<li><a href="/logout">Salir</a></li>
			</c:if>
        </ul>
      </div>
    </div>
  </nav>
</header>
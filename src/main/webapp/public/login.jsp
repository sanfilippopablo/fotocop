<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
	<head>
		<c:import url="/public/partials/head.html" charEncoding="UTF-8"/>
		<link rel="stylesheet" href="/public/styles/login.css" />
	</head>
	<body>

		<c:import url="/public/partials/header.html" charEncoding="UTF-8"/>

		<main class="grey lighten-3">
			<div class="container">
				<div class="row">
					<hgroup>
						<h3 class="center-align">Ingresar</h3>
						<h5 class="center-align teal-text">Usuario existente</h5>
					</hgroup>
				</div>
				<div class="row">
					<form action="/login" method="post" class="col s12 m6 l6 offset-m3 offset-l3 white" id="login">

						<input type="hidden" name="next" value="${next}" />

						<div class="row">
							<div class="input-field col s12">
								<input id="username" name="username" type="text" value="<c:out value='${param.username}' />" class="validate" />
								<label for="username">Usuario</label>
								<div class="error error-username">
										<p class="red-text"><c:out value="${validationManager.errorForField('username')}"/></p>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<input id="password" name="password" type="password" value="<c:out value='${param.password}' />" class="validate" />
								<label for="password">Contraseña</label>
								<div class="error error-password">
									<p class="red-text"><c:out value="${validationManager.errorForField('password')}"/></p>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col s12 center-align">
								<p class="red-text"><c:out value="${validationManager.errorForField('misc')}"/></p>
							</div>
						</div>

						<div class="row">
							<button class="waves-effect waves-teal btn btn-large col s12" type="submit" name="action">
								Iniciar Sesión
							</button>
						</div>
					</form>
				</div>
			</div>
		</main>

		<!-- Scripts -->
		<c:import url="/public/partials/scripts.html" charEncoding="UTF-8"/>
		<script src="/public/scripts/login.js" type="text/javascript"></script>
		
	</body>
</html>
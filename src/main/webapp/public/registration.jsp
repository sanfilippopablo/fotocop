<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
	<head>
		<c:import url="/public/partials/head.html" charEncoding="UTF-8"/>
		<link rel="stylesheet" href="/public/styles/registration.css" />
	</head>
	<body>

		<c:import url="/public/partials/header.html" charEncoding="UTF-8"/>

		<main class="grey lighten-3">
			<div class="container">
				<div class="row">
					<hgroup>
						<h3 class="center-align">Registro</h3>
						<h5 class="center-align teal-text">Nuevo usuario</h5>
					</hgroup>
				</div>
				<div class="row">
					<form class="col s12 m6 l6 offset-m3 offset-l3 white" id="register" method="POST" action="/registration">

						<div class="row">
							<div class="input-field col s12">
								<input id="username" name="username" type="text" class="validate" value="<c:out value='${param.username}' />" required />
								<label for="username">Usuario</label>
								<div class="error error-username">
									<p class="red-text"><c:out value="${validationManager.errorForField('username')}"/></p>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<input id="email" name="email" type="email" class="validate" value="<c:out value='${param.email}' />" required />
								<label for="email">Email</label>
								<div class="error error-email">
									<p class="red-text"><c:out value="${validationManager.errorForField('email')}"/></p>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m12 l6">
								<input id="password" name="password" type="password" class="validate" required />
								<label for="password">Contraseña</label>
								<div class="error error-password">
									<p class="red-text"><c:out value="${validationManager.errorForField('password')}"/></p>
								</div>
							</div>
							<div class="input-field col s12 m12 l6"	>
								<input id="password2" name="password2" type="password" class="validate" required />
								<label for="password2">Repita contraseña</label>
								<div class="error error-password2">
									<p class="red-text"><c:out value="${validationManager.errorForField('password2')}"/></p>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col s12 center-align">
								<p class="red-text"><c:out value="${validationManager.errorForField('misc')}"/></p>
							</div>
						</div>

						<div class="row">
							<button class="waves-effect waves-teal btn btn-large col s12" type="submit" name="action">
								Registrarme
							</button>
						</div>
					</form>
				</div>
			</div>
		</main>

		<!-- Scripts -->
		<c:import url="/public/partials/scripts.html" charEncoding="UTF-8"/>
  		<script src="/public/scripts/registration.js" type="text/javascript"></script>

	</body>
</html>
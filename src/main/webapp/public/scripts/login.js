$('document').ready(function()
{
  /* Validación */
  $("#login").validate({
    
    errorClass: 'invalid',
    validClass: "valid",
    
    errorPlacement: function (error, element) {
        element.next("label").attr("data-error", error.contents().text());
    },

    rules:
    {
      username:
      {
        required: true,
        maxlength: 14,
        minlength: 2
      },
      password:
      {
        required: true,
        minlength: 3,
        maxlength: 20
      }
    },

    messages:
    {
      password:
      {
        required: "Por favor, ingrese contraseña.",
        minlength: "Mínimo {0} caracteres",
        maxlength: "Máximo {0} caracteres"        
      },
      username: 
      {
        required: "Por favor, ingrese usuario.",
        minlength: "Mínimo {0} caracteres",
          maxlength: "Máximo {0} caracteres"
      }
    }
  });
  /* END Validation */

});
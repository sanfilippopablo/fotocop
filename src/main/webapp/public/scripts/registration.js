$('document').ready(function()
{
  /* Validación */
  $("#register").validate({

    errorClass: 'invalid',
    validClass: "valid",
    
    errorPlacement: function (error, element) {
        element.next("label").attr("data-error", error.contents().text());
    },

    rules:
    {
      username:
      {
        required: true,
        maxlength: 14,
        minlength: 2
      },
      email:
      {
        required: true,
        email: true
      },
      password:
      {
        required: true,
        minlength: 3,
        maxlength: 20
      },
      password2:
      {
        required: true,
        equalTo: "#password"
      }
    },
    messages:
    {
      password:
      {
        required: "Por favor, ingrese contraseña.",
        minlength: "Mínimo {0} caracteres.",
        maxlength: "Máximo {0} caracteres."
      },
      username: 
      {
    	  required: "Por favor, ingrese usuario.",
    	  minlength: "Mínimo {0} caracteres.",
          maxlength: "Máximo {0} caracteres."
      },
      password2: 
      {
    	  required: "Por favor, ingrese nuevamente la contraseña.",
    	  equalTo: "No son iguales las contraseñas."
      },
      email: 
      {
    	  required: "Por favor, ingrese email.",
    	  email: "No es un email valido."
      }
    }
  });
  /* END Validation */

});
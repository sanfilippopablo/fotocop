package jobs.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jobs.data.FilesService;
import jobs.data.JobsService;
import jobs.entities.File;
import jobs.entities.Job;
import jobs.entities.JobLine;
import users.entities.User;
import users.exceptions.AuthException;
import common.exceptions.BadRequestException;
import common.servlets.BaseServlet;

/**
 * Agrega un apunte de la biblioteca a un Job. Si no existe, se crea uno.
 *
 */

public class PrintApunteServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;
	public PrintApunteServlet() {
		// TODO Auto-generated constructor stub
		super();
	}
	/**
	 * Por GET tira error. No se permite por GET.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		throw new BadRequestException("Sólo por POST.");
	}
	/**
	 * Por POST ejecuta el behavior descrito arriba.
	 *
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {

		if (isLoggedIn(request)) {
			// Inicializamos los Services
			FilesService fs = new FilesService();
			JobsService jobsService = new JobsService();
      // Obtenemos la data por Post
      Integer fileID = Integer.parseInt(request.getParameter("fileID"));
      Integer quantity = Integer.parseInt(request.getParameter("newQuantity"));
			Integer abrochado = ("on".equals(request.getParameter("newAbrochado")))? 1 : 0;
			Integer anillado = ("on".equals(request.getParameter("newAnillado")))? 1 : 0;
			Integer dobleFaz = ("on".equals(request.getParameter("newDobleFaz")))? 1 : 0;
			User user = (User) request.getAttribute("user");

			// Identificamos el job correspondiente
			Job job = null;
      try {
          job = jobsService.getOpenJobForUser(user);
      } catch (SQLException | AuthException e) {
          throw new ServletException();
      }

      // Obtenemos el archivo
      File file;
	  try {
		file = fs.getFileById(fileID);
	  } catch (SQLException e1) {
		e1.printStackTrace();
		throw new ServletException();
	  }

      //Creamos la jobLine y le seteamos la metadata
      JobLine jobLine = new JobLine();
      jobLine.setFile(file);
      jobLine.setQuantity(quantity);
      jobLine.setAbrochado(abrochado.equals(1));
      jobLine.setDobleFaz(dobleFaz.equals(1));
      jobLine.setAnillado(anillado.equals(1));

      //La agregamos al Job
      try {
				Date eta = new Date();
				// Calculamos eta siendo igual al momento actual + lo que tarde en
        // imprimirse el actual trabajo + lo que tardan en imprimirse los
        // trabajos enviados
				long readyToPickupTime = job.getPrintingTime() + jobsService.getSentJobsTime();
				eta.setTime(eta.getTime() + readyToPickupTime);
				job.setEta(eta);
				jobsService.addJobLineToJob(job, jobLine);
			} catch (SQLException e) {
				e.printStackTrace();
			}
      response.sendRedirect("/job?id=" + job.getId());
		}

		else {
			// Not logged in. You shall not pass.
			redirectToLogin(request, response);
		}
	}
}

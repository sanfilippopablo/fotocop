package jobs.entities;

import java.util.ArrayList;

import users.entities.User;

/**
 * 
 * Representa un File / Archivo.
 *
 */

public class File {
	int id;
	String name;
	String title;
	ArrayList<String> tags;
	User uploader;
	
	long length;
	
	public long getLength() {
		return length;
	}
	
	public void setLength(long length) {
		this.length = length;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public User getUploader() {
		return uploader;
	}

	public void setUploader(User uploader) {
		this.uploader = uploader;
	}
	
	public boolean isPublic() {
		return this.uploader != null;
	}

}

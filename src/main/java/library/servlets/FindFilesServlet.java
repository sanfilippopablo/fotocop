package library.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import common.servlets.BaseServlet;

import jobs.data.FilesService;
import jobs.entities.File;
import library.data.LibraryService;
import users.entities.User;

/**
 * Sólo por GET. Descarga un File usando el campo id de requests.
 */
public class FindFilesServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FindFilesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (isLoggedIn(request)) {
			String query = request.getParameter("q");
			LibraryService ls = new LibraryService();
			try {
				ArrayList<File> files = ls.findFiles(query);
				request.setAttribute("files", files);
				request.getRequestDispatcher("/user/search-results.jsp").forward(request, response);
			} catch (SQLException e) {
				throw new ServletException();
			}	
		}
		else {
			// Not logged in. You shall not pass.
			redirectToLogin(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

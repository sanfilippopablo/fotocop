package library.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import common.servlets.BaseServlet;
import library.data.LibraryService;
import users.entities.User;

@MultipartConfig(location = "/tmp")

/**
 * Sólo por GET. Descarga un File usando el campo id de requests.
 */
public class UploadServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UploadServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		if (isLoggedIn(request)) {
			request.getRequestDispatcher("/user/upload-file.jsp").forward(request, response);	
		}
		else {
			// Not logged in. You shall not pass.
			redirectToLogin(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if (isLoggedIn(request)) {
			LibraryService ls = new LibraryService();
			// Obtenemos el nombre del archivo y su input stream
			Part part = request.getPart("newFile");
			String filename = getFileName(part);
			InputStream stream = part.getInputStream();
			// Asignamos variables a los atributos que necesitamos de la request
			String file_title = (String) request.getParameter("file_title");
			String file_tags = (String) request.getParameter("file_tags");
			User uploader = (User) request.getAttribute("user");
			// And now, we upload, baby
			try {
				ls.uploadFile(stream, filename, file_title, file_tags, uploader);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			request.getRequestDispatcher("/user/upload-success.jsp").forward(request, response);	
		}
		else {
			// Not logged in. You shall not pass.
			redirectToLogin(request, response);
		}		
	}

	/**
	 * Obtiene el nombre del archivo
	 */
	private String getFileName(Part part) {
		String partHeader = part.getHeader("content-disposition");
		for (String cd : partHeader.split(";")) {
			if (cd.trim().startsWith("filename")) {
				return cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
			}
		}
		return null;
	}
}

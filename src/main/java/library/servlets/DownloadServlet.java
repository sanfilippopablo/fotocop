package library.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import common.servlets.BaseServlet;

import jobs.data.FilesService;
import jobs.entities.File;
import users.entities.User;

/**
 * Sólo por GET. Descarga un File usando el campo id de requests.
 */
public class DownloadServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownloadServlet() {
      super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if (isLoggedIn(request)) {
			int fileId = Integer.parseInt(request.getParameter("id"));

			FilesService fs = new FilesService();
			File file = null;
			try {
				file = fs.getFileById(fileId);
			} catch (SQLException e) {
				// TODO Tirar 404
				e.printStackTrace();
				throw new ServletException(e);
			}
			User user = (User) request.getAttribute("user");
			//Si no es un archivo publico Y TAMPOCO es un archivo subido por quien lo quiere descargar, tira error
			if (!file.isPublic() && file.getUploader().getId() != user.getId()) {
				// TODO throw 403
				throw new ServletException("Forbidden");
			}

      // This line here would set the content length header, so the browser
      // knows the total length of the response and can show a progress bar
      // that goes from 0 to [length]. But, it's not working. For some reason,
      // it sets it to 0, so the browser stops downloading after 0 bytes.
      // By disabling this header setting, the browser doesn't know the size of the
      // download, but it downloads until the server stops sending data.
			// response.setContentLength((int) file.getLength());

      // Make sure to show the download dialog
      response.setHeader("Content-disposition","attachment; filename=" + file.getName());

      // This sends the file to browser
      OutputStream out = response.getOutputStream();
      InputStream in = fs.getInputStreamForFile(file);
      byte[] buffer = new byte[4096];
      int length;
      while ((length = in.read(buffer)) > 0){
         out.write(buffer, 0, length);
      }
      in.close();
      out.flush();
		}
		else {
			// Not logged in. You shall not pass.
			redirectToLogin(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

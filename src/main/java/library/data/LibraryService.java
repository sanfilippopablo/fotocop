package library.data;

import java.io.IOException;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import jobs.data.FilesService;
import jobs.entities.File;
import users.entities.User;

import com.mysql.jdbc.Statement;
import common.DBConnection;
import common.exceptions.NotFoundException;

public class LibraryService {

	/**
	 * Sube un apunte a la DB, a partir de un InputStream, su metadata, y un User (uploader)
	 *
	 * @param inputStream
	 * @param tags
	 * @param title
	 * @param name
	 * @throws IOException
	 * @throws SQLException
	 */
	public File uploadFile(InputStream inputStream, String name, String title, String tags, User uploader) throws SQLException, IOException{
		//Primero, creamos un objeto de tipo File con un FilesService
		FilesService fs = new FilesService();
		File file = fs.createFileFromInputStream(inputStream, name); //Este método también crea el File en la DB
		//Cargamos la metadata en la DB
		try(Connection connection = DBConnection.getConnection()) {
			String sql = "UPDATE files SET title = ?, uploader = ? WHERE id = ?";
			PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, title);
			ps.setInt(2, uploader.getId());
			ps.setInt(3, file.getId());
			ps.executeUpdate();
		}
		//Ahora, separamos los tags y los insertamos en la DB todos juntos
		ArrayList<String> processed_tags = identifyTagsFromString(tags);
		try(Connection connection = DBConnection.getConnection();
			PreparedStatement ps = connection.prepareStatement("INSERT INTO tags(file, tag) VALUES (?, ?);");
		) {
			for(String tag: processed_tags){
				ps.setInt(1, file.getId());
				ps.setString(2, tag);
				ps.addBatch(); //Agregamos este insert al Batch, para que después se inserten todos juntos
			}
			ps.executeBatch();
		}
		return file;
	}
	/**
	 * A partir de una string, devuelve el conjunto de tags
	 *
	 * @param input_string
	 */
	public ArrayList<String> identifyTagsFromString(String input_string){
		input_string = input_string.toLowerCase();
		String[] strings = input_string.split(" ");
		ArrayList<String> tags = new ArrayList<String>();
		for (String string : strings) {
			if (!string.isEmpty()) {
				tags.add(string);
			}
		}
		return tags;
	}

  /*
   * Devuleve un ArrayList de Files a partir de una query de búsqeda
   */
	public ArrayList<File> findFiles(String queryString) throws SQLException {
		String sqlQuery = "SELECT f.id, COUNT(f.id) matches "
					 + "FROM files f "
					 + "INNER JOIN tags t ON f.id = t.file "
					 + "WHERE t.`tag` IN ([tags]) "
					 + "OR f.title LIKE ? "
					 + "GROUP BY f.id "
					 + "ORDER BY matches DESC";
		ArrayList<String> tags = identifyTagsFromString(queryString);
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("?");
		for (int i = 1; i < tags.size(); i++) {
			queryBuilder.append(", ?");
		}
		sqlQuery = sqlQuery.replace("[tags]", queryBuilder.toString());
		try (
			Connection connection = DBConnection.getConnection();
			PreparedStatement ps = connection.prepareStatement(sqlQuery);
		) {
			int i = 1;
			for (String tag : tags) {
				ps.setString(i++, tag);
			}
			ps.setString(i++, "%" + queryString + "%");
			ResultSet rs = ps.executeQuery();
			ArrayList<File> files = new ArrayList<File>();
			FilesService fs = new FilesService();
			while (rs.next()) {
				Integer id = rs.getInt(1);
				File file;
				try {
					file = fs.getFileById(id);
					files.add(file);
				} catch (NotFoundException e) {
					e.printStackTrace();
				}
			}
			return files;
		}
	}
}

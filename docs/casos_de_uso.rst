###########################
Anexo: Casos de Uso
###########################


.. contents::
  :depth: 2
  :local:


*******************************************
CU 1: Registrar un nuevo usuario regular
*******************************************

Camino básico
====================

1. Usuario no registrado selecciona la opción "Nuevo usuario". Sistema muestra el form de nuevo usuario.
2. Usuario no registrado ingresa la información pedida (username, pass con confirmación, mail) y confirma. Sistema se encarga del registro del nuevo usuario e informa al usuario.

Alternativas
====================

2.A Mail no válido.
  2.A.1 Sistema informa situación (sin borrar la información ingresada).
2.B Las contraseñas no coinciden.
  2.B.1 Sistema informa situación (sin borrar la información ingresada).
2.C Username ya existe.
  2.C.1 Sistema informa situación (sin borrar la información ingresada).
2.D Ya hay un usuario asociado a ese mail.
  2.D.1 Sistema informa situación (sin borrar la información ingresada).

Notas
====================

No aplica.

***************************************************************
CU 2: Enviar a imprimir un apunte de la Biblioteca de Apuntes
***************************************************************

Camino básico
====================

1. Usuario clickea "Mandar a imprimir" en el Apunte que desea mandar a imprimir. Sistema crea un trabajo para el Usuario con el Apunte ya cargado como Archivo.
2. Usuario completa la metadata (cantidad de copias, preferencias de impresión, etc) y, si desea, agrega más archivos (desde su dispositivo o desde la Biblioteca) al trabajo.
3. Usuario clickea "Enviar". Sistema cambia el estado del Trabajo, lo envía a la lista de trabajos pendientes, y notifica al Usuario.

Alternativas
====================

1.A Ya hay un Trabajo abierto.
  1.A.1 Sistema agrega una línea de trabajo en el trabajo abierto, con el apunte cargado. Continúa CU.


Notas
====================
No aplica.

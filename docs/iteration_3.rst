###################
Etapa 3: Biblioteca
###################

.. contents::
  :depth: 2
  :local:


Visión general
**********************************

La tercer etapa, como indica el nombre, se encarga de la *Biblioteca de apuntes*. Es una plataforma virtual donde los usuarios de *Fotocop* pueden tanto subir sus apuntes para compartir con el resto, como navegar por la biblioteca (o buscar de acuerdo a *tags*) y descargar o mandar a imprimir cualquiera de ellos.

El algoritmo de búsqueda es la parte clave de esta etapa, ya que consideramos que es lo que marca la diferencia entre un sistema que funciona y un sistema bueno.

Casos de uso soportados
*************************

* **Subir apunte a la biblioteca /upload**

*Trigger: Luego de que el usuario clickeó "Subir apunte", se le abrió el modal, eligió el archivo y agregó tags, clickea "Subir".*

.. code-block:: java

  // Agrega a la DB de la biblioteca el apunte
  libraryService.uploadFile(File file, ArrayList<String> tags);

Redirigir a "Mis apuntes".

* **Buscar apunte /search**

*Trigger: Luego que el usuario escribió uno o más tags de búsqueda, apretó enter .*

.. code-block:: java

  // Extraer de la searchbox los tags
  ArrayList <String> tags = libraryService.identifyTagsFromString(String tagstream);
  // Obtener los apuntes que estén relacionados con esos tags
  ArrayList<Apuntes> apuntes = libraryService.searchByTags(ArrayList<String> tags);

Mostrar los apuntes que estén en ese ArrayList

* **Mandar a imprimir un apunte /addfiletojob/:apunteId**

Simplemente creamos un job para el usuario que esté en la sesión y en vez de que suba su propio file, le ponemos el file que haya elegido de la biblioteca. Se abre el modal de nuevo trabajo, con el file ya puesto, y completa la metadata.

.. code-block:: java

  // Crea el Job en la DB con el user asignado y lo devuelve.
  Job job = jobsService.createJobForUser(User user);

  //El file es el apunte dado
  File file = apunte;

  // Creamos la jobLine
  JobLine jobLine = new JobLine();
  jobLine.setFile(file);
  jobLine.setQuantity(quantity);
  // ... más set metadata

  // Agregar la jobLine al job. Este método lo agrega al objeto Java, pero además lo hace en la DB.
  jobsService.addJobLineToJob(job, jobLine);

###################
Etapa 2: Trabajos
###################

.. contents::
  :depth: 2
  :local:


Visión general
**********************************

Esta etapa se encarga de manejar los trabajos. Le da soporte a todo el sistema, desde el envío de trabajos a imprimir desde un usuario (con el cálculo de cuándo el usuario debe ir a retirar el trabajo listo), hasta el cambio de estado de los trabajos para indicar que han sido retirados, pasando por la llegada de dicho trabajo a la lista de trabajos pendientes de impresión y la descarga del mismo.

Es la etapa más grande de *Fotocop*, y también la más técnicamente compleja, debido al manejo de archivos y el cálculo de tiempo para retirar los trabajos listos.

Casos de uso soportados
*************************
* **Crear trabajo.**

*Trigger: Usuario clickea "Crear Trabajo"*

.. code-block:: java

    // Crea el Job en la DB con el user asignado y lo devuelve.
    jobsService.createJobForUser(User user);


Redirigir a página del trabajo.

* **Añadir archivo a trabajo.**


*Trigger: Luego que el usuario clickeó en Agregar archivo al trabajo, se le abrió el modal, completó el form con los datos del archivo más el trabajo, clickea Añadir.*

.. code-block:: java

  // Obtener el trabajo correspondiente
  Job job = jobsService.getJobById(id);

  // Obtenemos el stream del archivo que nos viene por POST y creamos un File
  // Este método guarda el archivo en el FS y la metadata (path y name) en la DB
  File file = filesService.createFileFromStream(Stream s, String filename);

  // Creamos la jobLine
  JobLine jobLine = new JobLine();
  jobLine.setFile(file);
  jobLine.setQuantity(quantity);
  // ... más set metadata

  // Agregar la jobLine al job. Este método lo agrega al objeto Java, pero además lo hace en la DB.
  jobsService.addJobLineToJob(job, jobLine);

* **Listar trabajos pendientes.**

Esto es el dashboard de los usuarios. Muestra los trabajos pendientes.

.. code-block:: java

  jobsService.getTrabajosPendientesForUser(User u);

* **Detalles de trabajo.**

Página de trabajo.

.. code-block:: java

  jobsService.getJobById(id);

* **Enviar trabajo.**

*Trigger: Luego de crear un trabajo y agregarle archivos, el user clickea Enviar.*

.. code-block:: java

  // submitJob updatea el status de abierto a enviado, calcula y setea un ETA, y guarda todo en la DB.
  jobsService.submitJob(job);
Redirigir luego al dashboard

* **Imprimir.**

*Trigger: Cuando un admin, en el admin dashboard, clickea Imprimir sobre algún trabajo.*

.. code-block:: java

  // En el servlet
  PrintService.printJob(job);
  job.setStatus("Printed");
  //Setear más metadata. Tal vez fecha y hora de impresión

  // PrintService.printJob
  for (JobLine jobLine: job.getLines()) {
      PrintService.printFile(jobLine.getFile())
  }

###################
Introducción
###################

.. contents::
  :depth: 2
  :local:


Objetivos y Alcance del Proyecto
**********************************

El Proyecto consiste en el desarrollo de un sistema JAVA dedicado a fotocopiadoras, llamado *Fotocop*. *Fotocop* ofrece una plataforma en internet para los usuarios (que necesitan registrarse, previo al uso del sistema) puedan mandar trabajos a imprimir desde su casa, o incluso elegir uno de los documentos existentes en la *biblioteca de apuntes*. También pueden agregar archivos a la biblioteca para que sea posible para otros usuarios verlos, descargarlos, y/o hacerlos imprimir.

Equipo de Trabajo
**********************
El equipo de trabajo está compuesto por tres personas.

* Pérez Fariña, Gonzalo. *Analista y Programador JAVA*
* Sanfilippo, Pablo. *Líder del proyecto y Programador JAVA*
* Segarra, Facundo. *Desarrollador del front-end y Programador JAVA*


Desarrollo del Sistema
==========================

Requerimientos alcanzados
**************************

1. Registrar un usuario regular.
2. Registrar un usuario administrador.
3. Enviar, desde un usuario regular, trabajo a imprimir.
4. Mostrar a un usuario administrador la lista de trabajos pendientes por imprimir.
5. Registrar, desde un usuario administrador, la entrega (pago) de un trabajo.
6. Subir, desde un usuario regular, apuntes a la biblioteca de apuntes.
7. Enviar, desde un usuario regular, apuntes de la biblioteca de apuntes a imprimir.

Modelo de dominio
********************

.. image:: modelo.jpg

Desglose de Trabajo
***************************

El equipo decidió dividir el sistema en bloques, que serán trabajados iterativamente, desde análisis hasta testing.
Los bloques en los que el trabajo se divide, junto con los requerimientos que cada uno soporta, son:

1. Alta de usuarios.
  * Registrar un usuario regular.
  * Registrar un usuario administrador.
  * Login al sistema.
2. Trabajos de impresión.
  * Enviar, desde un usuario regular, trabajo a imprimir.
  * Mostrar a un usuario administrador la lisat de trabajos pendientes por imprimir.
  * Registrar, desde un usuario administrador, la entrega (pago) de un trabajo.
3. Biblioteca de apuntes.
  * Subir, desde un usuario regular, apuntes a la biblioteca de apuntes.
  * Enviar, desde un usuario regular, apuntes de la biblioteca de apuntes a imprimir.

  Entorno de trabajo
  *********************

  INSERTAR TEXTO DE ENTORNO DE TRABAJO

Documentación de Fotocop
========================

Contenido:

.. toctree::
   :maxdepth: 2

   plan_de_proyecto
   iteration_1
   iteration_2
   iteration_3
   no_alcanzado
   casos_de_uso
   code-docs/packages

Índices y tablas
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

###################
Etapa 1: Usuarios
###################

.. contents::
  :depth: 2
  :local:


Visión general
**********************************

Esta etapa es la más básica de todas. Provee un servicio básico de registro e inicio de sesión de usuarios.

Casos de uso soportados
*************************
  * Registrar un usuario regular.
  * Registrar un usuario administrador.
  * Login al sistema.

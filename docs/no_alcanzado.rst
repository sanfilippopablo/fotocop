################################################
Casos de Uso no alcanzados y errores conocidos
################################################

.. contents::
  :depth: 2
  :local:


En esta sección se encuentran características no soportadas en esta versión de *Fotocop*. No se les dio soporte por diversas razones, pero principalmente es porque el proyecto adquiriría dimensiones no deseadas para la presentación de la materia.

Características de *Fotocop* NO alcanzadas
**********************************************

El Proyecto no alcanza (por ser un proyecto académico) pero sí debería alcanzar, para ser un proyecto comercialmente completo, las siguientes características:

* Baja de Apuntes en la Biblioteca
* Actualización de Apuntes en la Biblioteca
* Edición o eliminación de JobLines en un Job
* Cancelación de trabajos enviados
* Cambio de estado de los trabajos de "Terminado" a "Entregado"

Errores documentados
*************************

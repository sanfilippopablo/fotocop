#!/bin/bash

echo "=========== INSTALACION DE FRONTEND ============"

echo "--- Installing node.js ---"
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs build-essential git

echo "--- Installing Dependencias de NPM ---"
sudo npm install

echo "--- Installing Bower ---"
sudo npm install -g bower

echo "--- Installing Dependencias de Bower ---"
bower install
